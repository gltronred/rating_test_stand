#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
from collections import defaultdict, Counter
import pyexcel
import scipy.stats
import scipy.special
import numpy as np
from trueskill import Rating, rate_1vs1
from counter import RatingCounter, parse_date, tryfloat

CLASSNAME = "NaiveTrueskill"


def parse_date(date_):
    return datetime.datetime.strptime(date_, "%Y-%m-%d %H:%M:%S").date()


class NaiveTrueskill(RatingCounter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ratings = defaultdict(Rating)
        self.file_prefix = f"naive_trueskill_{self.ts}_"
        self.games_count = defaultdict(Counter)
        self.last_base = {}
        self.comparison = []

    @staticmethod
    def ts_rank(tsrating):
        return tsrating.mu - 3 * tsrating.sigma

    def compare_pearsonr(self, results_):
        results = [
            r
            for r in results_
            if tryfloat(r["position"]) and tryfloat(r["predicted_position"])
        ]
        true = [float(x["position"]) for x in results]
        l1 = [float(x["predicted_position"]) for x in results]
        l2 = scipy.stats.rankdata(
            [-self.ts_rank(self.ratings[x["idteam"]]) for x in results]
        )
        if len(l1) > 2 and len(l2) > 2:
            corr1 = scipy.stats.pearsonr(l1, true)[0]
            if np.isnan(corr1):
                corr1 = 0
            with np.errstate(all="ignore"):
                corr2 = scipy.stats.pearsonr(l2, true)[0]
                if np.isnan(corr2):
                    corr2 = 0
            self.comparison.append((corr1, corr2))

    def process_tournament(self, tournament):
        results = sorted(tournament["results"], key=lambda x: float(x["position"]))
        if "pearsonr" in (self.config.get("benchmarks") or []):
            self.compare_pearsonr(results)
        for team in tournament["recaps"]:
            if any([x["is_base"] == "1" for x in team["recaps"]]):
                self.last_base[team["idteam"]] = parse_date(tournament["date_end"])

        frozen_ratings = self.ratings.copy()
        for res in results:
            self.games_count[res["idteam"]][parse_date(tournament["date_end"])] += 1
            won_against_team = [
                r for r in results if float(r["position"]) < float(res["position"])
            ][-10:]
            lost_against_team = [
                r for r in results if float(r["position"]) > float(res["position"])
            ][:10]
            self.names[res["idteam"]] = res["base_name"]
            for t in won_against_team:
                _, self.ratings[res["idteam"]] = rate_1vs1(
                    frozen_ratings[t["idteam"]], self.ratings[res["idteam"]]
                )
            for t in lost_against_team:
                self.ratings[res["idteam"]], _ = rate_1vs1(
                    self.ratings[res["idteam"]], frozen_ratings[t["idteam"]]
                )

    def benchmark_pearsonr(self, target_date):
        if len(self.comparison) < self.config["benchmark_num"]:
            return
        last_n = self.comparison[-self.config["benchmark_num"] :]
        baseline = [x[0] for x in last_n]
        new = [x[1] for x in last_n]
        baseline_is_worse = scipy.stats.mannwhitneyu(baseline, new, alternative="less")[
            1
        ]
        baseline_is_better = scipy.stats.mannwhitneyu(
            baseline, new, alternative="greater"
        )[1]
        if baseline_is_better > 0.05 and baseline_is_worse > 0.05:
            pv = f"gray ({min(baseline_is_better, baseline_is_worse):.02})"
        elif baseline_is_better < 0.05:
            pv = f"↓ ({baseline_is_better:.02})"
        elif baseline_is_worse < 0.05:
            pv = f"↑ ({baseline_is_better:.02})"
        output = f"{target_date}: baseline: {np.mean(baseline):.02}, new: {np.mean(new):.02}, MW: {pv}"
        print(output)

    @staticmethod
    def calc_games(games_counter, threshold):
        result = 0
        for date in games_counter:
            if date < threshold:
                continue
            result += games_counter[date]
        return result

    def is_eligible(self, idteam, target_date):
        games_last_period = self.calc_games(
            self.games_count[idteam],
            target_date - datetime.timedelta(days=self.config["base_threshold"]),
        )
        has_base = (
            self.last_base.get(idteam)
            and (target_date - self.last_base[idteam]).days
            < self.config["base_threshold"]
        )
        base_special_case = int(idteam) in (49804, 670)  # БК, Ксеп
        return (has_base or base_special_case) and games_last_period > self.config[
            "games_threshold"
        ]

    def save_ratings(self):
        target_date = self.current_date + datetime.timedelta(self.step_days)
        top = sorted(
            [x for x in self.ratings if self.is_eligible(x, target_date)],
            key=lambda x: self.ts_rank(self.ratings[x]),
            reverse=True,
        )[: self.config["top_n_teams"]]
        dest_file_path = self.file_prefix + str(target_date) + ".xlsx"
        print(f"saving {dest_file_path}...")
        result_array = [["id", "name", "mu", "sigma", "rank"]]
        for t in top:
            result_array.append(
                [
                    t,
                    self.names.get(t) or "-",
                    self.ratings[t].mu,
                    self.ratings[t].sigma,
                    self.ts_rank(self.ratings[t]),
                ]
            )
        pyexcel.save_as(array=result_array, dest_file_name=dest_file_path)
        if "pearsonr" in (self.config.get("benchmarks") or []):
            self.benchmark_pearsonr(target_date)
