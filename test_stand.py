#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import datetime
import argparse
import importlib.util
import pickle
import json
import tqdm
import yaml


def load_rating_data(path):
    cwd = os.getcwd()
    os.chdir(path)
    with open("all_tournaments.json", encoding="utf8") as f:
        tournaments = json.load(f)
    for tournament in tqdm.tqdm(tournaments):
        id_ = tournament["idtournament"]
        with open(f"{id_}_metadata.json", encoding="utf8") as f:
            metadata = json.load(f)
        with open(f"{id_}_recaps.json", encoding="utf8") as f:
            recaps = json.load(f)
        with open(f"{id_}_list.json", encoding="utf8") as f:
            results = json.load(f)
        tournament["extended_metadata"] = metadata
        tournament["recaps"] = recaps
        tournament["results"] = results
    os.chdir(cwd)
    return tournaments


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--data-dir", "-d")
    parser.add_argument("--pickle-path", "-pp")
    parser.add_argument("--module-path", "-m")
    parser.add_argument("--config", "-c")
    parser.add_argument("--output-dir", "-o")
    args = parser.parse_args()

    if args.pickle_path:
        with open(args.pickle_path, "rb") as f:
            tournaments = pickle.load(f)
    else:
        tournaments = load_rating_data(args.data_dir)

    with open(args.config) as f:
        config = yaml.safe_load(f)

    spec = importlib.util.spec_from_file_location("um", args.module_path)
    um = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(um)
    rc = getattr(um, um.CLASSNAME)(
        tournaments=tournaments,
        config=config
    )
    os.chdir(os.path.abspath(args.output_dir))
    rc.count_ratings()


if __name__ == "__main__":
    main()
