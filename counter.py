import datetime


def parse_date(date_):
    return datetime.datetime.strptime(date_, "%Y-%m-%d %H:%M:%S").date()


def tryfloat(x):
    try:
        return float(x)
    except (ValueError, TypeError):
        return


class RatingCounter:
    def __init__(self, tournaments, config):
        self.tournaments = tournaments
        self.config = config
        self.start_date = self.config["start_date"]
        self.end_date = self.config["end_date"]
        self.step_days = self.config["step_days"]
        self.ratings = {}
        self.names = {}
        self.ts = datetime.datetime.now().strftime("%s")
        self.current_date = self.start_date

    def process_tournament(self, t):
        pass

    def save_ratings(self):
        pass

    def check_tournament(self, t):
        return (
            t["extended_metadata"][0]["tournament_in_rating"] == "1"
            and parse_date(t["date_end"]) >= self.current_date
            and parse_date(t["date_end"])
            < (self.current_date + datetime.timedelta(days=self.step_days))
        )

    def make_next_release(self):
        tournaments = [x for x in self.tournaments if self.check_tournament(x)]
        for tournament in tournaments:
            self.process_tournament(tournament)
        self.save_ratings()

    def count_ratings(self):
        while self.current_date < self.end_date:
            self.make_next_release()
            self.current_date += datetime.timedelta(self.step_days)
